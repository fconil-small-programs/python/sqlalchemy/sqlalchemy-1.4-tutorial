"""
https://docs.sqlalchemy.org/en/14/tutorial/index.html#unified-tutorial
https://docs.sqlalchemy.org/en/14/tutorial/metadata.html#table-reflection
"""
from sqlalchemy import MetaData
from sqlalchemy import Table
from sqlalchemy import create_engine

metadata = MetaData()

dbname = "sqlite+pysqlite:///some_table.db"
engine = create_engine(dbname, echo=True, future=True)

some_table = Table("some_table", metadata, autoload_with=engine)

if __name__ == "__main__":
    print(f"{some_table!r}")
