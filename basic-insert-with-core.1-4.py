"""
https://docs.sqlalchemy.org/en/14/tutorial/index.html#unified-tutorial

https://docs.sqlalchemy.org/en/14/tutorial/data_insert.html#tutorial-core-insert

ORM Readers - The way that rows are INSERTed into the database from an ORM
perspective makes use of object-centric APIs on the Session object known as the
unit of work process, and is fairly different from the Core-only approach
described here. The more ORM-focused sections later starting at Inserting Rows
with the ORM subsequent to the Expression Language sections introduce this.
"""
from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    MetaData,
    String,
    Table,
    bindparam,
    create_engine,
    insert,
    select,
)

metadata = MetaData()

user_table = Table(
    "user_account",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("name", String(30)),
    Column("fullname", String),
)

# When using the ForeignKey object within a Column definition, we can omit
# the datatype for that Column; it is automatically inferred from that of
# the related column.
address_table = Table(
    "address",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("user_id", ForeignKey("user_account.id"), nullable=False),
    Column("email_address", String, nullable=False),
)


def show_objects():
    print(user_table.c.name)
    print(user_table.c.keys())
    print(user_table.primary_key)


def emit_database_DDL(engine):
    # The DDL create process by default includes some SQLite-specific PRAGMA
    # statements that test for the existence of each table before emitting a
    # CREATE. The full series of steps are also included within a BEGIN/COMMIT
    # pair to accommodate for transactional DDL (SQLite does actually support
    # transactional DDL, however the sqlite3 database driver historically runs
    # DDL in “autocommit” mode).
    metadata.create_all(engine)


def insert_stmt(engine):
    stmt = insert(user_table).values(
        name="spongebob", fullname="Spongebob Squarepants"
    )
    print(f"stmt: {stmt}")

    # The stringified form is created by producing a Compiled form of the
    # object which includes a database-specific string SQL representation
    # of the statement; we can acquire this object directly using the
    # ClauseElement.compile() method
    compiled = stmt.compile()

    # Our Insert construct is an example of a “parameterized” construct,
    # illustrated previously at Sending Parameters; to view the name and
    # fullname bound parameters, these are available from the Compiled
    # construct as well
    print(f"compiled.params: {compiled.params}")

    with engine.connect() as conn:
        result = conn.execute(stmt)
        conn.commit()

        # In its simple form above, the INSERT statement does not return
        # any rows, and if only a single row is inserted, it will usually
        # include the ability to return information about column-level
        # default values that were generated during the INSERT of that row,
        # most commonly an integer primary key value.
        # CursorResult.inserted_primary_key returns a tuple because a
        # primary key may contain multiple columns.
        #  The CursorResult.inserted_primary_key is intended to always contain
        # the complete primary key of the record just inserted, not just a
        # “cursor.lastrowid” kind of value.
        print(f"result.inserted_primary_key: {result.inserted_primary_key}")


def insert_two_rows(engine):
    with engine.connect() as conn:
        result = conn.execute(
            insert(user_table),
            [
                {"name": "sandy", "fullname": "Sandy Cheeks"},
                {"name": "patrick", "fullname": "Patrick Star"},
            ],
        )
        conn.commit()


def scalar_subqueries(engine):
    # Deep Alchemy
    # A scalar subquery is constructed, making use of the select() construct
    # introduced in the next section, and the parameters used in the subquery
    # are set up using an explicit bound parameter name, established using the
    # bindparam() construct.
    #  This is some slightly deeper alchemy just so that we can add related rows
    # without fetching the primary key identifiers from the user_table operation
    # into the application. Most Alchemists will simply use the ORM which takes
    # care of things like this for us.
    # https://docs.sqlalchemy.org/en/14/glossary.html#term-scalar-subquery
    scalar_subquery = (
        select(user_table.c.id)
        .where(user_table.c.name == bindparam("username"))
        .scalar_subquery()
    )

    with engine.connect() as conn:
        result = conn.execute(
            insert(address_table).values(user_id=scalar_subquery),
            [
                {
                    "username": "spongebob",
                    "email_address": "spongebob@sqlalchemy.org",
                },
                {"username": "sandy", "email_address": "sandy@sqlalchemy.org"},
                {
                    "username": "sandy",
                    "email_address": "sandy@squirrelpower.org",
                },
            ],
        )
        conn.commit()


def insert_from_select(engine):
    # The Insert construct can compose an INSERT that gets rows directly from
    # a SELECT using the Insert.from_select() method.
    select_stmt = select(user_table.c.id, user_table.c.name + "@aol.com")
    insert_stmt = insert(address_table).from_select(
        ["user_id", "email_address"], select_stmt
    )
    print(insert_stmt)

    # with engine.connect() as conn:
    #     result = conn.execute(insert_stmt)
    #     conn.commit()

    #     print(f"result.inserted_primary_key: {result.inserted_primary_key}")
    #     print(f"result.rowcount: {result.rowcount}")
    #     print(f"result.lastrowid: {result.lastrowid}")


def insert_returning(engine):
    # The RETURNING clause for supported backends is used automatically in
    # order to retrieve the last inserted primary key value as well as the
    # values for server defaults. However the RETURNING clause may also be
    # specified explicitly using the Insert.returning() method; in this case,
    # the Result object that’s returned when the statement is executed has
    # rows which can be fetched.
    insert_stmt = insert(address_table).returning(
        address_table.c.id, address_table.c.email_address
    )
    print(insert_stmt)

    # with engine.connect() as conn:
    #     # sqlalchemy.exc.CompileError: RETURNING is not supported by this
    #     # dialect's statement compiler.
    #     result = conn.execute(insert_stmt)
    #     conn.commit()

    #     print(f"result.inserted_primary_key: {result.inserted_primary_key}")
    #     print(f"result.rowcount: {result.rowcount}")
    #     print(f"result.lastrowid: {result.lastrowid}")


def insert_from_select_with_returning(engine):
    # It can also be combined with Insert.from_select(), as in the example
    # below that builds upon the example stated in INSERT ... FROM SELECT
    #
    # The RETURNING feature is also supported by UPDATE and DELETE statements,
    # which will be introduced later in this tutorial. The RETURNING feature
    # is generally 1 only supported for statement executions that use a single
    # set of bound parameters; that is, it wont work with the “executemany”
    # form introduced at Sending Multiple Parameters.
    #
    # There is internal support for the psycopg2 dialect to INSERT many rows
    # at once and also support RETURNING, which is leveraged by the SQLAlchemy
    # ORM. However this feature has not been generalized to all dialects and
    # is not yet part of SQLAlchemy’s regular API.
    select_stmt = select(user_table.c.id, user_table.c.name + "@aol.com")
    insert_stmt = insert(address_table).from_select(
        ["user_id", "email_address"], select_stmt
    )
    print(
        insert_stmt.returning(address_table.c.id, address_table.c.email_address)
    )

    # with engine.connect() as conn:
    #     # sqlalchemy.exc.CompileError: RETURNING is not supported by this
    #     # dialect's statement compiler.
    #     result = conn.execute(
    #         insert_stmt.returning(
    #             address_table.c.id, address_table.c.email_address
    #         )
    #     )
    #     conn.commit()

    #     print(f"result.inserted_primary_key: {result.inserted_primary_key}")
    #     print(f"result.rowcount: {result.rowcount}")
    #     print(f"result.lastrowid: {result.lastrowid}")


if __name__ == "__main__":
    show_objects()

    dbname = "sqlite+pysqlite:///:memory:"
    engine = create_engine(dbname, echo=True, future=True)

    emit_database_DDL(engine)

    insert_stmt(engine)

    insert_two_rows(engine)

    scalar_subqueries(engine)

    # insert_from_select(engine)

    # insert_returning(engine)

    # insert_from_select_with_returning(engine)
