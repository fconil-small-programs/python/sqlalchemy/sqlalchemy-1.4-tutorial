"""
https://docs.sqlalchemy.org/en/14/tutorial/index.html#unified-tutorial

https://docs.sqlalchemy.org/en/14/tutorial/orm_related_objects.html

In this section, we will cover one more essential ORM concept, which is how the
ORM interacts with mapped classes that refer to other objects. In the section
Declaring Mapped Classes, the mapped class examples made use of a construct
called relationship(). This construct defines a linkage between two different
mapped classes, or from a mapped class to itself, the latter of which is called
a self-referential relationship.

Above, the User class now has an attribute User.addresses and the Address class
has an attribute Address.user. The relationship() construct will be used to
inspect the table relationships between the Table objects that are mapped to
the User and Address classes. As the Table object representing the address
table has a ForeignKeyConstraint which refers to the user_account table, the
relationship() can determine unambiguously that there is a one to many relationship
from User.addresses to User; one particular row in the user_account table may be
referred towards by many rows in the address table.

All one-to-many relationships naturally correspond to a many to one relationship
in the other direction, in this case the one noted by Address.user. The
relationship.back_populates parameter, seen above configured on both relationship()
objects referring to the other name, establishes that each of these two
relationship() constructs should be considered to be complimentary to each other.
"""
from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    String,
    create_engine,
    select,
    text,
)
from sqlalchemy.orm import (
    Session,
    aliased,
    contains_eager,
    joinedload,
    registry,
    relationship,
    selectinload,
    session,
    with_parent,
)

mapper_registry = registry()

# The above registry, when constructed, automatically includes a MetaData
# object that will store a collection of Table objects.
print(mapper_registry.metadata)

# Instead of declaring Table objects directly, we will now declare them
# indirectly through directives applied to our mapped classes. In the most
# common approach, each mapped class descends from a common base class known
# as the declarative base. We get a new declarative base from the registry
# using the registry.generate_base() method.
Base = mapper_registry.generate_base()


class User(Base):
    __tablename__ = "user_account"

    # Column objects are assigned to class-level attributes within the classes
    # These Column objects can usually be declared without an explicit “name”
    # field inside the constructor, as the Declarative process will name them
    # automatically based on the attribute name that was used.
    id = Column(Integer, primary_key=True)
    name = Column(String(30))
    fullname = Column(String)

    addresses = relationship("Address", back_populates="user")

    def __repr__(self):
        return f"User(id={self.id!r}, name={self.name!r}, fullname={self.fullname!r})"


class Address(Base):
    __tablename__ = "address"

    id = Column(Integer, primary_key=True)
    email_address = Column(String, nullable=False)
    user_id = Column(Integer, ForeignKey("user_account.id"))

    user = relationship("User", back_populates="addresses")

    def __repr__(self):
        return f"Address(id={self.id!r}, email_address={self.email_address!r})"


class RUser(Base):
    __tablename__ = "r_user_account"

    # Column objects are assigned to class-level attributes within the classes
    # These Column objects can usually be declared without an explicit “name”
    # field inside the constructor, as the Declarative process will name them
    # automatically based on the attribute name that was used.
    id = Column(Integer, primary_key=True)
    name = Column(String(30))
    fullname = Column(String)

    addresses = relationship(
        "RAddress", back_populates="user", lazy="raise_on_sql"
    )

    def __repr__(self):
        return f"RUser(id={self.id!r}, name={self.name!r}, fullname={self.fullname!r})"


class RAddress(Base):
    __tablename__ = "r_address"

    id = Column(Integer, primary_key=True)
    email_address = Column(String, nullable=False)
    user_id = Column(Integer, ForeignKey("r_user_account.id"))

    user = relationship(
        "RUser", back_populates="addresses", lazy="raise_on_sql"
    )

    def __repr__(self):
        return f"RAddress(id={self.id!r}, email_address={self.email_address!r})"


def show_objects():
    print(f"{User.__table__!r}")


def emit_database_DDL(engine):
    # Emit CREATE statements given ORM registry
    mapper_registry.metadata.create_all(engine)


def insert_first_users(engine):
    # The insert tutorial is in insert_data() function
    with Session(engine) as session:
        with session.begin():
            bob = User(name="spongebob", fullname="Spongebob Squarepants")
            bob.addresses.append(
                Address(email_address="spongebob@sqlalchemy.org")
            )
            sandy = User(name="sandy", fullname="Sandy Cheeks")
            sandy.addresses.append(
                Address(email_address="sandy@sqlalchemy.org")
            )
            sandy.addresses.append(
                Address(email_address="sandy@squirrelpower.org")
            )
            patrick = User(name="patrick", fullname="Patrick Star")
            squidward = User(name="squidward", fullname="Squidward Tentacles")

            session.add(bob)
            session.add(sandy)
            session.add(patrick)
            session.add(squidward)


def persisting_and_loading_relationships(engine):
    # We can start by illustrating what relationship() does to instances of
    # objects. If we make a new User object, we can note that there is a Python
    # list when we access the .addresses element.
    session = Session(engine)

    u1 = User(name="pkrabs", fullname="Eugene H. Krabs")

    # This object is a SQLAlchemy-specific version of Python list which has the
    # ability to track and respond to changes made to it. The collection also
    # appeared automatically when we accessed the attribute, even though we
    # never assigned it to the object.
    # This is similar to the behavior noted at Inserting Rows with the ORM
    # where it was observed that column-based attributes to which we don’t
    # explicitly assign a value also display as None automatically, rather than
    # raising an AttributeError as would be Python’s usual behavior.
    # As the u1 object is still transient and the list that we got from
    # u1.addresses has not been mutated (i.e. appended or extended), it’s not
    # actually associated with the object yet, but as we make changes to it, it
    # will become part of the state of the User object.
    print(f"u1: {u1}\nu1.addresses: {u1.addresses}")

    a1 = Address(email_address="pearl.krabs@gmail.com")
    print(f"a1: {a1}\na1.user: {a1.user}")

    u1.addresses.append(a1)

    # As we associated the Address object with the User.addresses collection of
    # the u1 instance, another behavior also occurred, which is that the
    # User.addresses relationship synchronized itself with the Address.user
    # relationship, such that we can navigate not only from the User object to
    # the Address object, we can also navigate from the Address object back to
    # the “parent” User object.
    print(f"u1.addresses: {u1.addresses}\na1.user: {a1.user}")

    # This synchronization occurred as a result of our use of the
    # relationship.back_populates parameter between the two relationship()
    # objects. This parameter names another relationship() for which
    # complementary attribute assignment / list mutation should occur. It will
    # work equally well in the other direction, which is that if we create
    # another Address object and assign to its Address.user attribute, that
    # Address becomes part of the User.addresses collection on that User object.
    a2 = Address(email_address="pearl@aol.com", user=u1)
    print(f"a2: {a2}\na2.user: {a2.user}\nu1.addresses: {u1.addresses}")

    # Use of the user parameter as a keyword argument in the Address
    # constructor is equivalent to assignment of the Address.user attribute.
    # a3 = Address(email_address="pearl@hotmail.com")
    # a3.user = u1
    # print(f"a3: {a3}\na3.user: {a3.user}\nu1.addresses: {u1.addresses}")

    #  Cascading Objects into the Session
    # ----------------------------------
    # We now have a User and two Address objects that are associated in a
    # bidirectional structure in memory, but as noted previously in Inserting
    # Rows with the ORM , these objects are said to be in the transient state
    # until they are associated with a Session object.
    # We make use of the Session that’s still ongoing, and note that when we
    # apply the Session.add() method to the lead User object, the related
    # Address object also gets added to that same Session
    session.add(u1)

    print(f"u1 in session: {u1 in session}")
    print(f"a1 in session: {a1 in session} ; a2 in session: {a2 in session}")
    # print(f"a3 in session: {a3 in session}")

    # The above behavior, where the Session received a User object, and
    # followed along the User.addresses relationship to locate a related
    # Address object, is known as the save-update cascade and is discussed in
    # detail in the ORM reference documentation at Cascades.

    # The three objects are now in the pending state; this means they are ready
    # to be the subject of an INSERT operation but this has not yet proceeded;
    # all three objects have no primary key assigned yet.
    print(f"u1.id: {u1.id} ; a1.user_id: {a1.user_id}")

    #  It’s at this stage that we can see the very great utility that the unit
    # of work process provides; recall in the section INSERT usually generates
    # the “values” clause automatically, rows were inserted into the
    # user_account and address tables using some elaborate syntaxes in order to
    # automatically associate the address.user_id columns with those of the
    # user_account rows. Additionally, it was necessary that we emit INSERT for
    # user_account rows first, before those of address, since rows in address
    # are dependent on their parent row in user_account for a value in their
    # user_id column.

    # When using the Session, all this tedium is handled for us and even the
    # most die-hard SQL purist can benefit from automation of INSERT, UPDATE
    # and DELETE statements. When we Session.commit() the transaction all steps
    # invoke in the correct order, and furthermore the newly generated primary
    # key of the user_account row is applied to the address.user_id column
    # appropriately.
    session.commit()

    #  Loading Relationships
    # ---------------------

    # When we next access an attribute on these objects, we’ll see the SELECT
    # emitted for the primary attributes of the row, such as when we view the
    # newly generated primary key for the u1 object.
    print(f"u1.id: {u1.id}")

    # The u1 User object now has a persistent collection User.addresses that we
    # may also access. As this collection consists of an additional set of rows
    # from the address table, when we access this collection as well we again
    # see a lazy load emitted in order to retrieve the objects.
    print(f"u1.addresses: {u1.addresses}")

    #  Collections and related attributes in the SQLAlchemy ORM are persistent
    # in memory; once the collection or attribute is populated, SQL is no
    # longer emitted until that collection or attribute is expired. We may
    # access u1.addresses again as well as add or remove items and this will
    # not incur any new SQL calls.
    print(f"u1.addresses: {u1.addresses}")

    # While the loading emitted by lazy loading can quickly become expensive
    # if we don’t take explicit steps to optimize it, the network of lazy
    # loading at least is fairly well optimized to not perform redundant work;
    # as the u1.addresses collection was refreshed, per the identity map these
    # are in fact the same Address instances as the a1 and a2 objects we’ve
    # been dealing with already, so we’re done loading all attributes in this
    # particular object graph.
    print(f"a1: {a1} ; a2: {a2}")


def using_relationships_in_queries(engine):
    # The previous section introduced the behavior of the relationship()
    # construct when working with instances of a mapped class, above, the u1,
    # a1 and a2 instances of the User and Address classes. In this section, we
    # introduce the behavior of relationship() as it applies to class level
    # behavior of a mapped class, where it serves in several ways to help
    # automate the construction of SQL queries.

    session = Session(engine)

    # In order to describe how to join between tables, these methods either
    # infer the ON clause based on the presence of a single unambiguous
    # ForeignKeyConstraint object within the table metadata structure that
    # links the two tables, or otherwise we may provide an explicit SQL
    # Expression construct that indicates a specific ON clause.

    # Using Relationships to Join
    # ---------------------------

    # When using ORM entities, an additional mechanism is available to help us
    # set up the ON clause of a join, which is to make use of the
    # relationship() objects that we set up in our user mapping. The class-bound
    # attribute corresponding to the relationship() may be passed as the single
    # argument to Select.join(), where it serves to indicate both the right side
    # of the join as well as the ON clause at once.
    stmt = select(Address.email_address).select_from(User).join(User.addresses)
    print(f"Relationships stmt: {stmt}")

    # The presence of an ORM relationship() on a mapping is not used by
    # Select.join() or Select.join_from() if we don’t specify it; it is not
    # used for ON clause inference.
    # This means, if we join from User to Address without an ON clause, it
    # works because of the ForeignKeyConstraint between the two mapped Table
    # objects, not because of the relationship() objects on the User and
    # Address classes.
    stmt = select(Address.email_address).join_from(User, Address)
    print(f"ForeignKeyConstraint stmt: {stmt}")

    # Joining between Aliased targets
    # -------------------------------
    # In the section ORM Entity Aliases we introduced the aliased() construct,
    # which is used to apply a SQL alias to an ORM entity. When using a
    # relationship() to help construct SQL JOIN, the use case where the target
    # of the join is to be an aliased() is suited by making use of the
    # PropComparator.of_type() modifier. To demonstrate we will construct the
    # same join illustrated at ORM Entity Aliases using the relationship()
    # attributes to join instead.
    address_alias_1 = aliased(Address)
    address_alias_2 = aliased(Address)

    stmt = (
        select(User)
        .join(User.addresses.of_type(address_alias_1))
        .where(address_alias_1.email_address == "patrick@aol.com")
        .join(User.addresses.of_type(address_alias_2))
        .where(address_alias_2.email_address == "patrick@gmail.com")
    )
    print(f"Aliased stmt: {stmt}")

    # To make use of a relationship() to construct a join from an aliased
    # entity, the attribute is available from the aliased() construct
    # directly.
    user_alias_1 = aliased(User)
    stmt = select(user_alias_1.name).join(user_alias_1.addresses)
    print(f"Aliased stmt: {stmt}")

    # Augmenting the ON Criteria
    # --------------------------

    # The ON clause generated by the relationship() construct may also be
    # augmented with additional criteria. This is useful both for quick ways to
    # limit the scope of a particular join over a relationship path, and also
    # for use cases like configuring loader strategies.The PropComparator.and_()
    # method accepts a series of SQL expressions positionally that will be
    # joined to the ON clause of the JOIN via AND. For example if we wanted to
    # JOIN from User to Address but also limit the ON criteria to only certain
    # email addresses.
    stmt = select(User.fullname).join(
        User.addresses.and_(Address.email_address == "pearl.krabs@gmail.com")
    )
    result = session.execute(stmt)
    print(result.all())

    # EXISTS forms: has() / any()
    # ---------------------------

    # In the section EXISTS subqueries, we introduced the Exists object that
    # provides for the SQL EXISTS keyword in conjunction with a scalar
    # subquery. The relationship() construct provides for some helper methods
    # that may be used to generate some common EXISTS styles of queries in
    # terms of the relationship.
    #  For a one-to-many relationship such as User.addresses, an EXISTS against
    # the address table that correlates back to the user_account table can be
    # produced using PropComparator.any(). This method accepts an optional
    # WHERE criteria to limit the rows matched by the subquery.
    stmt = select(User.fullname).where(
        User.addresses.any(Address.email_address == "pearl.krabs@gmail.com")
    )
    print(session.execute(stmt).all())

    # As EXISTS tends to be more efficient for negative lookups, a common query
    # is to locate entities where there are no related entities present. This
    # is succinct using a phrase such as ~User.addresses.any(), to select for
    # User entities that have no related Address rows.
    stmt = select(User.fullname).where(~User.addresses.any())
    print(session.execute(stmt).all())

    #  The PropComparator.has() method works in mostly the same way as
    # PropComparator.any(), except that it’s used for many-to-one relationships,
    # such as if we wanted to locate all Address objects which belonged to “pearl”.
    stmt = select(Address.email_address).where(
        Address.user.has(User.name == "pkrabs")
    )
    print(session.execute(stmt).all())

    # Common Relationship Operators
    # -----------------------------

    u1 = session.get(User, 5)
    # print(f"u1: {u1}")
    a1 = session.get(Address, 1)
    # print(f"a1: {a1}")

    # There are some additional varieties of SQL generation helpers that come
    # with relationship(), including :

    # many to one equals comparison
    # A specific object instance can be compared to many-to-one relationship,
    # to select rows where the foreign key of the target entity matches the
    # primary key value of the object given
    stmt = select(Address).where(Address.user == u1)
    print(f"many to one equals comparison => stmt: {stmt}")

    # many to one not equals comparison
    # The not equals operator may also be used:
    stmt = select(Address).where(Address.user != u1)
    print(f"many to one equals comparison => stmt: {stmt}")

    # object is contained in a one-to-many collection
    # This is essentially the one-to-many version of the “equals” comparison,
    # select rows where the primary key equals the value of the foreign key in
    # a related object.
    stmt = select(User).where(User.addresses.contains(a1))
    print(f"object is contained in a one-to-many collection => stmt: {stmt}")
    # print(session.execute(stmt).all())

    # An object has a particular parent from a one-to-many perspective
    # The with_parent() function produces a comparison that returns rows which
    # are referred towards by a given parent, this is essentially the same as
    # using the == operator with the many-to-one side.
    # MAIS C'EST BEAUCOUP PLUS COMPLIQUE A ECRIRE ET A COMPRENDRE
    stmt = select(Address).where(with_parent(u1, User.addresses))
    print(
        f"an object has a particular parent from a one-to-many perspective => stmt: {stmt}"
    )
    # print(session.execute(stmt).all())


def strategy_selectinload(engine):
    # https://docs.sqlalchemy.org/en/14/tutorial/orm_related_objects.html#selectin-load

    # The most useful loader in modern SQLAlchemy is the selectinload() loader
    # option. This option solves the most common form of the “N plus one”
    # problem which is that of a set of objects that refer to related
    # collections. selectinload() will ensure that a particular collection for
    # a full series of objects are loaded up front using a single query. It
    # does this using a SELECT form that in most cases can be emitted against
    # the related table alone, without the introduction of JOINs or subqueries,
    # and only queries for those parent objects for which the collection isn’t
    # already loaded. Below we illustrate selectinload() by loading all of the
    # User objects and all of their related Address objects; while we invoke
    # Session.execute() only once, given a select() construct, when the database
    # is accessed, there are in fact two SELECT statements emitted, the second
    # one being to fetch the related Address objects.
    session = Session(engine)

    stmt = select(User).options(selectinload(User.addresses)).order_by(User.id)

    for row in session.execute(stmt):
        print(
            f"selectinload: {row.User.name} ({', '.join(a.email_address for a in row.User.addresses)})"
        )


def stategy_joinedload(engine):
    # https://docs.sqlalchemy.org/en/14/tutorial/orm_related_objects.html#joined-load

    # The joinedload() eager load strategy is the oldest eager loader in
    # SQLAlchemy, which augments the SELECT statement that’s being passed to
    # the database with a JOIN (which may be an outer or an inner join
    # depending on options), which can then load in related objects.
    #  The joinedload() strategy is best suited towards loading related
    # many-to-one objects, as this only requires that additional columns are
    # added to a primary entity row that would be fetched in any case. For
    # greater effiency, it also accepts an option joinedload.innerjoin so that
    # an inner join instead of an outer join may be used for a case such as
    # below where we know that all Address objects have an associated User.
    session = Session(engine)

    stmt = (
        select(Address)
        .options(joinedload(Address.user, innerjoin=True))
        .order_by(Address.id)
    )

    for row in session.execute(stmt):
        print(
            f"joinedload: {row.Address.email_address} {row.Address.user.name}"
        )


def strategy_explicit_join_and_eager_load(engine):
    # https://docs.sqlalchemy.org/en/14/tutorial/orm_related_objects.html#explicit-join-eager-load

    session = Session(engine)

    # If we were to load Address rows while joining to the user_account table
    # using a method such as Select.join() to render the JOIN, we could also
    # leverage that JOIN in order to eagerly load the contents of the
    # Address.user attribute on each Address object returned. This is
    # essentially that we are using “joined eager loading” but rendering the
    # JOIN ourselves. This common use case is acheived by using the
    # contains_eager() option. This option is very similar to joinedload(),
    # except that it assumes we have set up the JOIN ourselves, and it instead
    # only indicates that additional columns in the COLUMNS clause should be
    # loaded into related attributes on each returned object, for example
    stmt = (
        select(Address)
        .join(Address.user)
        .where(User.name == "pkrabs")
        .options(contains_eager(Address.user))
        .order_by(Address.id)
    )

    for row in session.execute(stmt):
        print(
            f"join and contains_eager: {row.Address.email_address} {row.Address.user.name}"
        )

    # Above, we both filtered the rows on user_account.name and also loaded
    # rows from user_account into the Address.user attribute of the returned
    # rows. If we had applied joinedload() separately, we would get a SQL query
    # that unnecessarily joins twice.
    stmt = (
        select(Address)
        .join(Address.user)
        .where(User.name == "pkrabs")
        .options(joinedload(Address.user))
        .order_by(Address.id)
    )

    # SELECT has a JOIN and LEFT OUTER JOIN unnecessarily
    print(f"bad joinedload alternative => stmt: {stmt}")


def augmenting_loader_strategy_paths(engine):
    #  https://docs.sqlalchemy.org/en/14/tutorial/orm_related_objects.html#augmenting-loader-strategy-paths

    # In Augmenting the ON Criteria we illustrated how to add arbitrary
    # criteria to a JOIN rendered with relationship() to also include
    # additional criteria in the ON clause. The PropComparator.and_() method
    # is in fact generally available for most loader options. For example, if
    # we wanted to re-load the names of users and their email addresses, but
    # omitting the email addresses with the sqlalchemy.org domain, we can apply
    # PropComparator.and_() to the argument passed to selectinload() to limit
    # this criteria.
    session = Session(engine)

    stmt = (
        select(User)
        .options(
            selectinload(
                User.addresses.and_(
                    ~Address.email_address.endswith("sqlalchemy.org")
                )
            )
        )
        .order_by(User.id)
        .execution_options(populate_existing=True)
    )

    # A very important thing to note above is that a special option is added
    # with .execution_options(populate_existing=True). This option which takes
    # effect when rows are being fetched indicates that the loader option we
    # are using should replace the existing contents of collections on the
    # objects, if they are already loaded. As we are working with a single
    # Session repeatedly, the objects we see being loaded above are the same
    # Python instances as those that were first persisted at the start of the
    # ORM section of this tutorial.
    # ====> CE N'EST PAS MON CAS CAR J'AI DÉCOUPE LE TUTORIEL EN PLUSIEURS FONCTIONS
    for row in session.execute(stmt):
        print(
            f"loader_strategy_paths: {row.User.name} ({', '.join(a.email_address for a in row.User.addresses)})"
        )


def insert_rtables_for_raiseload(engine):
    with engine.begin() as conn:
        conn.execute(
            text(
                "INSERT INTO r_user_account (name, fullname) VALUES (:name, :fullname)"
            ),
            [{"name": "spongebob", "fullname": "Spongebob Squarepants"}],
        )

        conn.execute(
            text(
                "INSERT INTO r_address (email_address, user_id) VALUES (:email_address, :user_id)"
            ),
            [{"email_address": "spongebob@sqlalchemy.org", "user_id": 1}],
        )

        result = conn.execute(
            text(
                "SELECT u.name, u.fullname, a.email_address FROM r_user_account AS u JOIN r_address as a on a.user_id=u.id"
            )
        )
        for row in result:
            print(
                f"[raiseload data] name: {row.name}, fullname: {row.fullname}, email_address: {row.email_address}"
            )


def strategy_raiseload(engine):
    # https://docs.sqlalchemy.org/en/14/tutorial/orm_related_objects.html#raiseload

    session = Session(engine)

    #  One additional loader strategy worth mentioning is raiseload(). This
    # option is used to completely block an application from having the N plus
    # one problem at all by causing what would normally be a lazy load to raise
    # an error instead. It has two variants that are controlled via the
    # raiseload.sql_only option to block either lazy loads that require SQL,
    # versus all “load” operations including those which only need to consult
    # the current Session.

    #  One way to use raiseload() is to configure it on relationship() itself,
    # by setting relationship.lazy to the value "raise_on_sql", so that for a
    # particular mapping, a certain relationship will never try to emit SQL.

    # VOIR relationship() POUR LES CLASSES PARTICULIERES RUser ET RAddress
    u1 = session.execute(select(RUser)).scalars().first()
    # print(f"u1.addresses: {u1.addresses}")
    # sqlalchemy.exc.InvalidRequestError: 'RUser.addresses' is not available due to lazy='raise_on_sql'

    #  The exception would indicate that this collection should be loaded up
    # front instead:
    u1 = (
        session.execute(select(RUser).options(selectinload(RUser.addresses)))
        .scalars()
        .first()
    )
    print(f"u1.addresses: {u1.addresses}")

    # The lazy="raise_on_sql" option tries to be smart about many-to-one
    # relationships as well; above, if the Address.user attribute of an Address
    # object were not loaded, but that User object were locally present in the
    # same Session, the “raiseload” strategy would not raise an error.

    # PAS SIMPLE A GERER, COMMENT ON PROCEDE A L'INSERTION ???


if __name__ == "__main__":
    show_objects()

    dbname = "sqlite+pysqlite:///:memory:"
    engine = create_engine(dbname, echo=True, future=True)

    emit_database_DDL(engine)

    insert_first_users(engine)

    persisting_and_loading_relationships(engine)

    using_relationships_in_queries(engine)

    # JE PENSE QU'IL FAUDRA ETRE CONFRONTEE AUX USE CASES POUR BIEN COMPRENDRE
    # CES DIFFERENTES STRATEGIES
    strategy_selectinload(engine)

    stategy_joinedload(engine)

    strategy_explicit_join_and_eager_load(engine)

    augmenting_loader_strategy_paths(engine)

    insert_rtables_for_raiseload(engine)
    strategy_raiseload(engine)
