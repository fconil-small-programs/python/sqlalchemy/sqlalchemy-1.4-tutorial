"""
https://docs.sqlalchemy.org/en/14/tutorial/index.html#unified-tutorial
https://docs.sqlalchemy.org/en/14/tutorial/metadata.html#combining-core-table-declarations-with-orm-declarative

Combinaison de basic-metadata.1-4.py avec basic-metadata-with-orm.1-4.py

The most common foundational objects for database metadata in SQLAlchemy are
known as MetaData, Table, and Column.
"""
# from sqlalchemy import MetaData
from sqlalchemy import Table, Column, Integer, String
from sqlalchemy import ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import registry
from sqlalchemy.orm import relationship

# metadata = MetaData()
mapper_registry = registry()
Base = mapper_registry.generate_base()

user_table = Table(
    "user_account",
    mapper_registry.metadata,
    Column("id", Integer, primary_key=True),
    Column("name", String(30)),
    Column("fullname", String),
)

# When using the ForeignKey object within a Column definition, we can omit
# the datatype for that Column; it is automatically inferred from that of
# the related column.
address_table = Table(
    "address",
    mapper_registry.metadata,
    Column("id", Integer, primary_key=True),
    Column("user_id", ForeignKey("user_account.id"), nullable=False),
    Column("email_address", String, nullable=False),
)


class User(Base):
    # This form is called hybrid table, and it consists of assigning to the
    # .__table__ attribute directly, rather than having the declarative process
    # generate it.
    # The traditional “declarative base” approach using __tablename__ to
    # automatically generate Table objects remains the most popular method to
    # declare table metadata.
    # ATTENTION : IT USES __table__ NOT __tablename__ !!!
    __table__ = user_table

    addresses = relationship("Address", back_populates="user")

    def __repr__(self):
        return f"User({self.name!r}, {self.fullname!r})"


class Address(Base):
    __table__ = address_table

    user = relationship("User", back_populates="addresses")

    def __repr__(self):
        return f"Address({self.email_address!r})"


def show_objects():
    print(f"{User.__table__!r}")
    print(user_table.c.keys())
    print(user_table.primary_key)


def emit_database_DDL(engine):
    # Emit CREATE statements given ORM registry
    mapper_registry.metadata.create_all(engine)

    # The identical MetaData object is also present on the declarative base
    # Base.metadata.create_all(engine)


if __name__ == "__main__":
    show_objects()

    dbname = "sqlite+pysqlite:///:memory:"
    engine = create_engine(dbname, echo=True, future=True)

    emit_database_DDL(engine)
