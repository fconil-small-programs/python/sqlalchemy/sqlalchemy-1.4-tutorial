"""
https://docs.sqlalchemy.org/en/14/tutorial/index.html#unified-tutorial
https://docs.sqlalchemy.org/en/14/tutorial/metadata.html#tutorial-working-with-metadata

When using the ORM, the process by which we declare Table metadata is usually
combined with the process of declaring mapped classes. The mapped class is any
Python class we’d like to create, which will then have attributes on it that
will be linked to the columns in a database table. While there are a few varieties
of how this is achieved, the most common style is known as declarative, and
allows us to declare our user-defined classes and Table metadata at once.

When using the ORM, the MetaData collection remains present, however it itself
is contained within an ORM-only object known as the registry. We create a
registry by constructing it.
"""
from sqlalchemy import Column, Integer, String
from sqlalchemy import ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import registry
from sqlalchemy.orm import relationship

mapper_registry = registry()

# The above registry, when constructed, automatically includes a MetaData
# object that will store a collection of Table objects.
print(mapper_registry.metadata)

# Instead of declaring Table objects directly, we will now declare them
# indirectly through directives applied to our mapped classes. In the most
# common approach, each mapped class descends from a common base class known
# as the declarative base. We get a new declarative base from the registry
# using the registry.generate_base() method.
Base = mapper_registry.generate_base()

# The steps of creating the registry and “declarative base” classes can be
# combined into one step using the historically familiar declarative_base()
# function.
# THEN WHERE IS THE REGISTRY AND HOW TO ACCESS IT
# from sqlalchemy.orm import declarative_base
# Base = declarative_base()


class User(Base):
    __tablename__ = "user_account"

    # Column objects are assigned to class-level attributes within the classes
    # These Column objects can usually be declared without an explicit “name”
    # field inside the constructor, as the Declarative process will name them
    # automatically based on the attribute name that was used.
    id = Column(Integer, primary_key=True)
    name = Column(String(30))
    fullname = Column(String)

    addresses = relationship("Address", back_populates="user")

    def __repr__(self):
        return f"User(id={self.id!r}, name={self.name!r}, fullname={self.fullname!r})"


class Address(Base):
    __tablename__ = "address"

    id = Column(Integer, primary_key=True)
    email_address = Column(String, nullable=False)
    user_id = Column(Integer, ForeignKey("user_account.id"))

    user = relationship("User", back_populates="addresses")

    def __repr__(self):
        return f"Address(id={self.id!r}, email_address={self.email_address!r})"


def show_objects():
    # The above two classes are now our mapped classes, and are available for
    # use in ORM persistence and query operations.
    # But they also include Table objects that were generated as part of the
    # declarative mapping process, and are equivalent to the ones that we
    # declared directly in the previous Core section.
    # We can see these Table objects from a declarative mapped class using the
    # .__table__ attribute
    print(f"{User.__table__!r}")


def emit_database_DDL(engine):
    # Emit CREATE statements given ORM registry
    mapper_registry.metadata.create_all(engine)

    # The identical MetaData object is also present on the declarative base
    # Base.metadata.create_all(engine)


if __name__ == "__main__":
    show_objects()

    dbname = "sqlite+pysqlite:///:memory:"
    engine = create_engine(dbname, echo=True, future=True)

    emit_database_DDL(engine)
