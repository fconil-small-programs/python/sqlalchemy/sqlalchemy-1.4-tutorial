from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import declarative_base, scoped_session, sessionmaker

Base = declarative_base()


class Author(Base):
    __tablename__ = "authors"
    id = Column(Integer, primary_key=True)
    name = Column(String)


if __name__ == "__main__":
    dbname = "sqlite:///session.db"
    # dbname = "sqlite:///:memory:"
    engine = create_engine(dbname)  # , echo=True)

    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    # The sessionmaker factory generates new Session objects when called,
    # creating them given the configurational arguments established here.

    # La classe sessionmaker() retourne une **instance** de sessionmaker
    # cf sessionmaker.__class__ dans sqlalchemy/orm/session.py
    session_factory = sessionmaker(bind=engine)

    # Provides scoped management of Session objects.
    # https://docs.sqlalchemy.org/en/13/orm/contextual.html#thread-local-scope

    # La classe scoped_session() crée une **instance** de scoped_session
    # qui utilise une **instance** de sessionmaker pour créer une
    # instance de la classe Session()
    scoped_session_factory = scoped_session(session_factory)

    session = scoped_session_factory()

    author = Author(name="René Barjavel")

    session.add(author)
    session.commit()
    session.close()
