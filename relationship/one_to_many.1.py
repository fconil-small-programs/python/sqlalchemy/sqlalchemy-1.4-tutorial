"""
https://docs.sqlalchemy.org/en/14/orm/basic_relationships.html
"""
from sqlalchemy import create_engine
from sqlalchemy import Column, ForeignKey, Integer
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy.orm.session import Session

Base = declarative_base()


class Parent(Base):
    __tablename__ = "parent"

    id = Column(Integer, primary_key=True)
    children = relationship("Child")


class Child(Base):
    __tablename__ = "child"

    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey("parent.id"))


if __name__ == "__main__":
    engine = create_engine("sqlite:///one_to_many.1.db", echo=False)

    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    # cf ../basic-session.1.4.py
    with Session(engine) as session:
        with session.begin():
            pass
