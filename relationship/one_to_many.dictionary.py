"""
https://docs.sqlalchemy.org/en/14/orm/basic_relationships.html
https://docs.sqlalchemy.org/en/14/orm/collections.html#customizing-collection-access

ATTENTION aux changements de la clé du dictionnaire
https://docs.sqlalchemy.org/en/14/orm/collections.html#dealing-with-key-mutations-and-back-populating-for-dictionary-collections
"""
from sqlalchemy import create_engine
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy.orm.collections import attribute_mapped_collection
from sqlalchemy.orm.session import Session

Base = declarative_base()


class Item(Base):
    __tablename__ = "item"

    id = Column(Integer, primary_key=True)
    notes = relationship(
        "Note",
        collection_class=attribute_mapped_collection("keyword"),
        backref="item",
        cascade="all, delete-orphan",
    )

    def __repr__(self):
        return f"<Item (name={self.id}, notes={self.notes})>"


class Note(Base):
    __tablename__ = "note"

    id = Column(Integer, primary_key=True)
    keyword = Column(String)
    text = Column(String)

    item_id = Column(Integer, ForeignKey("item.id"))

    def __repr__(self):
        return f"<Node (keyword={self.keyword}, text={self.text})>"


if __name__ == "__main__":
    engine = create_engine("sqlite:///one_to_many.dictionary.db", echo=False)

    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    # cf ../basic-session.1.4.py
    with Session(engine) as session:
        item = Item()
        n1 = Note(keyword="a", text="atext")
        n1.item = item
        n2 = Note(keyword="b", text="btext")
        n2.item = item

        with session.begin():
            session.add(item)
            session.add(n1)
            session.add(n2)

        print(item)
        # <Item (name=1, notes={'a': <Node (keyword=a, text=atext)>, 'b': <Node (keyword=b, text=btext)>})>  # noqa E501

        print(n1)
        # <Node (keyword=a, text=atext)>
