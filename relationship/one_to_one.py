"""
https://docs.sqlalchemy.org/en/14/orm/basic_relationships.html

Pourquoi ne pas utiliser ce que Nicolas m'a indiqué avec "unique" ?
=> Si c'est évoqué dans le 3ème paragraphe

The “one-to-one” convention is achieved by applying a value of False
to the relationship.uselist parameter of the relationship() construct,

As mentioned previously, the ORM considers the “one-to-one” pattern as
a convention, where it makes the assumption that when it loads the
Parent.child attribute on a Parent object, it will get only one row back.
If more than one row is returned, the ORM will emit a warning.

However, the Child.parent side of the above relationship remains as a
“many-to-one” relationship and is unchanged, and there is no intrinsic
system within the ORM itself that prevents more than one Child object
to be created against the same Parent during persistence.
Instead, techniques such as unique constraints may be used in the actual
database schema to enforce this arrangement, where a unique constraint on
the Child.parent_id column would ensure that only one Child row may refer
to a particular Parent row at a time.
"""

from sqlalchemy import create_engine
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy.orm.session import Session

Base = declarative_base()


class Parent(Base):
    __tablename__ = "parent"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    child = relationship("Child", back_populates="parent", uselist=False)

    def __repr__(self):
        return f"<Parent (name={self.name})>"


class Child(Base):
    __tablename__ = "child"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    # TODO : ajouter une contrainte "unique" sur la colonne
    parent_id = Column(Integer, ForeignKey("parent.id"), unique=True)

    parent = relationship("Parent", back_populates="child")

    def __repr__(self):
        return f"<Child (name={self.name}, parent={self.parent})>"


if __name__ == "__main__":
    engine = create_engine("sqlite:///one_to_one.db", echo=False)

    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    # cf ../basic-session.1.4.py
    with Session(engine) as session:
        with session.begin():
            jack = Parent(name="Jack")
            session.add(jack)

            john = Child(name="John")
            john.parent = jack
            session.add(john)

            print(jack)
            print(john)
