"""
https://docs.sqlalchemy.org/en/14/orm/basic_relationships.html

Je n'arrive pas à comprendre la différence avec l'exemple one-to-many
puisque dans ce cas on semble avoir plusieurs parents associés à un enfant.
"""

from sqlalchemy import create_engine
from sqlalchemy import Column, ForeignKey, Integer
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy.orm.session import Session

Base = declarative_base()


class Parent(Base):
    __tablename__ = "parent"

    id = Column(Integer, primary_key=True)
    child_id = Column(Integer, ForeignKey("child.id"))
    child = relationship("Child", back_populates="parents")


class Child(Base):
    __tablename__ = "child"

    id = Column(Integer, primary_key=True)

    parents = relationship("Parent", back_populates="child")


if __name__ == "__main__":
    engine = create_engine("sqlite:///many_to_one.db", echo=False)

    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    # cf ../basic-session.1.4.py
    with Session(engine) as session:
        with session.begin():
            pass
