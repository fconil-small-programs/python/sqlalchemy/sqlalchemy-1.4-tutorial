"""
https://docs.sqlalchemy.org/en/14/orm/tutorial.html#building-a-relationship
https://docs.sqlalchemy.org/en/14/orm/basic_relationships.html

The two complementing relationships Address.user and User.addresses are referred
to as a bidirectional relationship, and is a key feature of the SQLAlchemy ORM.
"""
from sqlalchemy import create_engine
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy.orm.session import Session

Base = declarative_base()


class Address(Base):
    __tablename__ = "addresses"

    id = Column(Integer, primary_key=True)
    email_address = Column(String, nullable=True)
    user_id = Column(Integer, ForeignKey("users.id"))

    user = relationship("User", back_populates="addresses")

    def __repr__(self):
        return (
            f"<Address (email_address={self.email_address}, user={self.user})>"
        )


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    fullname = Column(String)
    nickname = Column(String)

    addresses = relationship(
        "Address", order_by="Address.id", back_populates="user"
    )

    def __repr__(self):
        return (
            f"<User (name={self.name}, fullname={self.fullname},"
            f"nickname={self.nickname}, addresses={self.addresses})>"
        )


if __name__ == "__main__":
    engine = create_engine("sqlite:///building_a_relationship.db", echo=False)

    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    # cf ../basic-session.1.4.py
    with Session(engine) as session:
        with session.begin():
            ed_user = User(
                name="ed", fullname="Ed Jones", nickname="edsnickname"
            )
            session.add(ed_user)

            jack = User(name="jack", fullname="Jack Bean", nickname="gjffdd")

            addr1 = Address(email_address="jack@google.com")
            addr2 = Address(email_address="j25@yahoo.com")
            jack.addresses = [addr1, addr2]

            session.add(jack)

            print(jack)
            print(addr1)
