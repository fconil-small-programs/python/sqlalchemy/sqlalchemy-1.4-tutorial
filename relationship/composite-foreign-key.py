"""
https://stackoverflow.com/questions/43051051/using-sqlalchemy-with-composite-primary-and-foreign-keys

https://docs.sqlalchemy.org/en/14/orm/declarative_tables.html?highlight=__table_args__#declarative-table-configuration

https://docs.sqlalchemy.org/en/14/orm/basic_relationships.html
https://docs.sqlalchemy.org/en/14/faq/ormconfiguration.html#i-m-using-declarative-and-setting-primaryjoin-secondaryjoin-using-an-and-or-or-and-i-am-getting-an-error-message-about-foreign-keys
https://docs.sqlalchemy.org/en/14/orm/relationship_persistence.html
"""
from uuid import uuid4

from slugify import slugify
from sqlalchemy import (
    Column,
    ForeignKey,
    ForeignKeyConstraint,
    Text,
    create_engine,
)
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy.orm.session import Session

Base = declarative_base()


class Org(Base):
    __tablename__ = "org"

    name = Column(Text)
    orgid = Column(Text, primary_key=True)

    projects = relationship("Project", back_populates="org")

    def __init__(self, name):
        self.name = name
        self.orgid = slugify(name)

    def __repr__(self):
        return f"<Org (name={self.name}, orgid={self.orgid}, projects={self.projects})>"


class Project(Base):
    __tablename__ = "project"

    name = Column(Text)
    orgid = Column(Text, ForeignKey("org.orgid"), primary_key=True)
    projectid = Column(Text, primary_key=True)

    org = relationship("Org", back_populates="projects")

    streams = relationship("Stream", back_populates="project")

    def __init__(self, name):
        self.name = name
        self.projectid = slugify(name)

    def __repr__(self):
        return (
            f"<Project (name={self.name}, projectid={self.projectid}, "
            f"streams={self.streams})>"
        )


class Stream(Base):
    __tablename__ = "stream"

    uuid = Column(Text, primary_key=True)
    name = Column(Text, index=True, nullable=False)

    orgid = Column(Text)
    projectid = Column(Text)

    project = relationship(
        "Project", foreign_keys=[orgid, projectid], back_populates="streams"
    )

    __table_args__ = (
        ForeignKeyConstraint(
            ["orgid", "projectid"], ["project.orgid", "project.projectid"]
        ),
    )

    def __init__(self):
        self.uuid = str(uuid4())

    def __repr__(self):
        return (
            f"<Stream (name={self.name}, uuid={self.uuid}, "
            f"orgid={self.orgid}, projectid={self.projectid})>"
        )


if __name__ == "__main__":
    engine = create_engine("sqlite:///composite-foreign-key.db", echo=False)

    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    # cf ../basic-session.1.4.py
    with Session(engine) as session:
        o1 = Org(name="Test des clés composites")
        o2 = Org(name="This ist ein beispiel")

        p1 = Project(name="Die straße")
        p1.org = o1

        p2 = Project(name="Die straße")
        p2.org = o2

        s1 = Stream()
        s1.name = "Il faut maintenant relier le flux au project"
        s1.project = p1

        s2 = Stream()
        s2.name = "Je manque d'inspiration"
        s2.project = p2

        with session.begin():
            session.add(o1)
            session.add(o2)

            session.add(p1)
            session.add(p2)

            session.add(s1)
            session.add(s2)

        print(o1)
        print(o2)
        print(p1)
        print(p2)
        print(s1)
        print(s2)
