"""
https://docs.sqlalchemy.org/en/14/orm/basic_relationships.html

Many to Many adds an association table between two classes. The association
table is indicated by the relationship.secondary argument to relationship().

Usually, the Table uses the MetaData object associated with the declarative
base class, so that the ForeignKey directives can locate the remote tables
with which to link.

It is also recommended, though not in any way required by SQLAlchemy, that
the columns which refer to the two entity tables are established within either
a unique constraint or more commonly as the primary key constraint; this
ensures that duplicate rows won’t be persisted within the table regardless of
issues on the application side.

DELETE PB

https://docs.sqlalchemy.org/en/14/orm/basic_relationships.html#deleting-rows-from-the-many-to-many-table
https://docs.sqlalchemy.org/en/14/orm/cascades.html#passive-deletes
https://docs.sqlalchemy.org/en/14/orm/relationship_api.html

In order to use ON DELETE foreign key cascades in conjunction with
relationship(), it’s important to note first and foremost that the
relationship.cascade setting must still be configured to match the
desired “delete” or “set null” behavior (using delete cascade or
leaving it omitted).

Le comportement du DELETE est plus clair dans la page suivante :

https://docs.sqlalchemy.org/en/14/orm/session_basics.html#deleting
"""

from sqlalchemy import create_engine
from sqlalchemy import Column, ForeignKey, Integer, String, Table
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy.orm.session import Session


Base = declarative_base()

parent_child_table = Table(
    "parent_child",
    Base.metadata,
    # Create a composed primary key : PRIMARY KEY (parent_id, child_id)
    Column(
        "parent_id",
        ForeignKey("parent.id", ondelete="CASCADE"),
        primary_key=True,
    ),
    Column(
        "child_id", ForeignKey("child.id", ondelete="CASCADE"), primary_key=True
    ),
)


class Parent(Base):
    __tablename__ = "parent"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    children = relationship(
        "Child",
        secondary=parent_child_table,
        back_populates="parents",
        cascade="all, delete",
    )

    def __repr__(self):
        return f"<Parent (name={self.name}, children={self.children})>"  # , children={self.children})>"  # noqa E501


class Child(Base):
    __tablename__ = "child"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    parents = relationship(
        "Parent",
        secondary=parent_child_table,
        back_populates="children",
        passive_deletes=True,
    )

    def __repr__(self):
        return f"<Child (name={self.name})>"  # , parents={self.parents})>"


if __name__ == "__main__":
    engine = create_engine("sqlite:///many_to_many.db", echo=False)

    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    # cf ../basic-session.1.4.py
    with Session(engine) as session:
        jack = Parent(name="Jack")
        wendy = Parent(name="Wendy")
        jim = Parent(name="Jim")

        john = Child(name="John")
        alice = Child(name="Alice")
        john.parents = [jack, wendy]
        alice.parents = [wendy, jim]

        with session.begin():
            session.add(jack)
            session.add(wendy)
            session.add(jim)

            session.add(john)
            session.add(alice)

        with session.begin():
            # wendy.children.remove(john)
            session.delete(alice)

        print(wendy)
        print(john)
