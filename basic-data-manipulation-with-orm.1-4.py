"""
https://docs.sqlalchemy.org/en/14/tutorial/index.html#unified-tutorial

https://docs.sqlalchemy.org/en/14/tutorial/orm_data_manipulation.html
"""
from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    String,
    create_engine,
    delete,
    select,
    update,
)
from sqlalchemy.orm import Session, registry, relationship

mapper_registry = registry()

# The above registry, when constructed, automatically includes a MetaData
# object that will store a collection of Table objects.
print(mapper_registry.metadata)

# Instead of declaring Table objects directly, we will now declare them
# indirectly through directives applied to our mapped classes. In the most
# common approach, each mapped class descends from a common base class known
# as the declarative base. We get a new declarative base from the registry
# using the registry.generate_base() method.
Base = mapper_registry.generate_base()


class User(Base):
    __tablename__ = "user_account"

    # Column objects are assigned to class-level attributes within the classes
    # These Column objects can usually be declared without an explicit “name”
    # field inside the constructor, as the Declarative process will name them
    # automatically based on the attribute name that was used.
    id = Column(Integer, primary_key=True)
    name = Column(String(30))
    fullname = Column(String)

    addresses = relationship("Address", back_populates="user")

    def __repr__(self):
        return f"User(id={self.id!r}, name={self.name!r}, fullname={self.fullname!r})"


class Address(Base):
    __tablename__ = "address"

    id = Column(Integer, primary_key=True)
    email_address = Column(String, nullable=False)
    user_id = Column(Integer, ForeignKey("user_account.id"))

    user = relationship("User", back_populates="addresses")

    def __repr__(self):
        return f"Address(id={self.id!r}, email_address={self.email_address!r})"


def show_objects():
    print(f"{User.__table__!r}")


def emit_database_DDL(engine):
    # Emit CREATE statements given ORM registry
    mapper_registry.metadata.create_all(engine)


def insert_first_users(engine):
    # The insert tutorial is in insert_data() function
    with Session(engine) as session:
        with session.begin():
            bob = User(name="spongebob", fullname="Spongebob Squarepants")
            session.add(bob)

            sandy = User(name="sandy", fullname="Sandy Cheeks")
            session.add(sandy)

            patrick = User(name="patrick", fullname="Patrick Star")
            session.add(patrick)


def insert_data(engine):
    # When using the ORM, the Session object is responsible for constructing
    # Insert constructs and emitting them for us in a transaction. The way we
    # instruct the Session to do so is by adding object entries to it; the
    # Session then makes sure these new entries will be emitted to the database
    # when they are needed, using a process known as a flush.

    # Instances of Classes represent Rows
    # -----------------------------------

    # With the ORM we make direct use of the custom Python classes we defined.
    # These classes also serve as extensible data objects that we use to create
    # and manipulate rows within a transaction as well.
    # Below we will create two User objects each representing a potential database
    # row to be INSERTed.
    squidward = User(name="squidward", fullname="Squidward Tentacles")
    krabs = User(name="ehkrabs", fullname="Eugene H. Krabs")

    # In a similar manner as in our Core examples of Insert, we did not include
    # a primary key (i.e. an entry for the id column), since we would like to
    # make use of the auto-incrementing primary key feature of the database,
    # SQLite in this case, which the ORM also integrates with. The value of the
    # id attribute on the above objects, if we were to view it, displays itself
    # as None.
    print(f"User squidward not yet inserted: {squidward}")

    # The None value is provided by SQLAlchemy to indicate that the attribute
    # has no value as of yet. SQLAlchemy-mapped attributes always return a
    # value in Python and don’t raise AttributeError if they’re missing, when
    # dealing with a new object that has not had a value assigned.
    #
    # At the moment, our two objects above are said to be in a state called
    # transient - they are not associated with any database state and are yet
    # to be associated with a Session object that can generate INSERT statements
    # for them.

    # Adding objects to a Session
    #  ---------------------------

    # To illustrate the addition process step by step, we will create a Session
    # without using a context manager (and hence we must make sure we close it later!).
    session = Session(engine)

    # The objects are then added to the Session using the Session.add() method.
    # When this is called, the objects are in a state known as pending and have
    # not been inserted yet.
    session.add(squidward)
    session.add(krabs)

    # When we have pending objects, we can see this state by looking at a
    # collection on the Session called Session.new
    # The above view is using a collection called IdentitySet that is
    # essentially a Python set that hashes on object identity in all cases
    # (i.e., using Python built-in id() function, rather than the Python hash()
    # function).
    print(f"Session pending objects: {session.new}")

    # Flushing
    # --------

    # The Session makes use of a pattern known as unit of work.
    # https://docs.sqlalchemy.org/en/14/glossary.html#term-unit-of-work
    # This generally means it accumulates changes one at a time, but does not
    # actually communicate them to the database until needed. This allows it to
    # make better decisions about how SQL DML should be emitted in the
    # transaction based on a given set of pending changes. When it does emit
    # SQL to the database to push out the current set of changes, the process
    # is known as a flush.
    session.flush()

    # Above we observe the Session was first called upon to emit SQL, so it
    # created a new transaction and emitted the appropriate INSERT statements
    # for the two objects. The transaction now remains open until we call any
    # of the Session.commit(), Session.rollback(), or Session.close() methods
    # of Session.
    #
    # While Session.flush() may be used to manually push out pending changes
    # to the current transaction, it is usually unnecessary as the Session
    # features a behavior known as autoflush, which we will illustrate later.
    # It also flushes out changes whenever Session.commit() is called.

    # Autogenerated primary key attributes
    # ------------------------------------

    # Once the rows are inserted, the two Python objects we’ve created are in
    # a state known as persistent, where they are associated with the Session
    # object in which they were added or loaded, and feature lots of other
    # behaviors that will be covered later.
    print(f"squidward.id: {squidward.id}")
    print(f"krabs.id: {krabs.id}")

    # Why did the ORM emit two separate INSERT statements when it could have
    # used executemany? As we’ll see in the next section, the Session when
    # flushing objects always needs to know the primary key of newly inserted
    # objects. If a feature such as SQLite’s autoincrement is used (other
    # examples include PostgreSQL IDENTITY or SERIAL, using sequences, etc.),
    # the CursorResult.inserted_primary_key feature usually requires that each
    # INSERT is emitted one row at a time. If we had provided values for the
    # primary keys ahead of time, the ORM would have been able to optimize the
    # operation better. Some database backends such as psycopg2 can also INSERT
    # many rows at once while still being able to retrieve the primary key values.

    # Identity Map
    # ------------
    #  The primary key identity of the objects are significant to the Session,
    # as the objects are now linked to this identity in memory using a feature
    # known as the identity map. The identity map is an in-memory store that
    # links all objects currently loaded in memory to their primary key identity.
    # We can observe this by retrieving one of the above objects using the
    # Session.get() method, which will return an entry from the identity map if
    # locally present, otherwise emitting a SELECT.
    some_squiward = session.get(User, 4)

    print(f"some_squiward: {some_squiward}")

    # The important thing to note about the identity map is that it maintains
    # a unique instance of a particular Python object per a particular database
    # identity, within the scope of a particular Session object.
    print(f"some_squiward is squiward: {some_squiward is squidward}")

    # Committing
    # ----------
    session.commit()


def updating_and_deleting_orm_objects(engine):
    # Updating ORM Objects
    #  --------------------

    # In the preceding section Updating and Deleting Rows with Core, we
    # introduced the Update construct that represents a SQL UPDATE statement.
    # When using the ORM, there are two ways in which this construct is used.
    # The primary way is that it is emitted automatically as part of the unit
    # of work process used by the Session, where an UPDATE statement is emitted
    # on a per-primary key basis corresponding to individual objects that have
    # changes on them. A second form of UPDATE is called an “ORM enabled UPDATE”
    # and allows us to use the Update construct with the Session explicitly;
    # this is described in the next section.

    session = Session(engine)

    #  Supposing we loaded the User object for the username sandy into a
    # transaction (also showing off the Select.filter_by() method as well as
    # the Result.scalar_one() method)
    sandy = session.execute(select(User).filter_by(name="sandy")).scalar_one()

    # The Python object sandy as mentioned before acts as a proxy for the row
    # in the database, more specifically the database row in terms of the current
    # transaction, that has the primary key identity of 2
    print(f"sandy: {sandy}")

    #  If we alter the attributes of this object, the Session tracks this change.
    sandy.fullname = "Sandy Squirrel"

    # The object appears in a collection called Session.dirty, indicating the
    # object is “dirty”.
    print(f"sandy in session.dirty: {sandy in session.dirty}")

    #  When the Session next emits a flush, an UPDATE will be emitted that
    # updates this value in the database. As mentioned previously, a flush
    # occurs automatically before we emit any SELECT, using a behavior known
    # as autoflush. We can query directly for the User.fullname column from
    # this row and we will get our updated value back.
    sandy_fullname = session.execute(
        select(User.fullname).where(User.id == 2)
    ).scalar_one()
    print(f"sandy_fullname: {sandy_fullname}")

    # The sandy Python object is now no longer considered dirty
    print(f"sandy in session.dirty: {sandy in session.dirty}")

    # However note we are still in a transaction and our changes have not been
    # pushed to the database’s permanent storage.

    # ORM-enabled UPDATE statements
    # -----------------------------

    # As previously mentioned, there’s a second way to emit UPDATE statements
    # in terms of the ORM, which is known as an ORM enabled UPDATE statement.
    # This allows the use of a generic SQL UPDATE statement that can affect
    # many rows at once.
    session.execute(
        update(User)
        .where(User.name == "sandy")
        .values(fullname="Sandy Squirrel Extraordinaire")
    )

    # When invoking the ORM-enabled UPDATE statement, special logic is used
    # to locate objects in the current session that match the given criteria,
    # so that they are refreshed with the new data. Above, the sandy object
    # identity was located in memory and refreshed.
    #
    # The refresh logic is known as the synchronize_session option, and is
    # described in detail in the section UPDATE and DELETE with arbitrary
    # WHERE clause.
    print(f"sandy.fullname: {sandy.fullname}")

    # Mais attention pas les valeurs récupérées spécifiquement
    print(f"sandy_fullname: {sandy_fullname}")

    # Deleting ORM Objects
    # --------------------

    patrick = session.get(User, 3)
    print(f"patrick: {patrick}")

    # If we mark patrick for deletion, as is the case with other operations,
    # nothing actually happens yet until a flush proceeds.
    session.delete(patrick)

    # Current ORM behavior is that patrick stays in the Session until the flush
    # proceeds, which as mentioned before occurs if we emit a query.
    session.execute(select(User).where(User.name == "patrick")).first()

    # Above, the SELECT we asked to emit was preceded by a DELETE, which
    # indicated the pending deletion for patrick proceeded. There was also a
    # SELECT against the address table, which was prompted by the ORM looking
    # for rows in this table which may be related to the target row; this
    # behavior is part of a behavior known as cascade, and can be tailored
    # to work more efficiently by allowing the database to handle related rows
    # in address automatically; the section delete has all the detail on this.

    # Beyond that, the patrick object instance now being deleted is no longer
    # considered to be persistent within the Session.
    print(f"patrick in session: {patrick in session}")

    # However just like the UPDATEs we made to the sandy object, every change
    # we’ve made here is local to an ongoing transaction, which won’t become
    # permanent if we don’t commit it.

    # ORM-enabled DELETE Statements
    # -----------------------------
    squidward = session.get(User, 4)

    print(f"squidward: {squidward}")

    # Like UPDATE operations, there is also an ORM-enabled version of DELETE
    # which we can illustrate by using the delete() construct with
    # Session.execute(). It also has a feature by which non expired objects
    # (see expired) that match the given deletion criteria will be
    # automatically marked as “deleted” in the Session.
    session.execute(delete(User).where(User.name == "squidward"))

    # The squidward identity, like that of patrick, is now also in a deleted
    # state. Note that we had to re-load squidward above in order to
    # demonstrate this; if the object were expired, the DELETE operation would
    # not take the time to refresh expired objects just to see that they had
    # been deleted.
    print(f"squidward in session: {squidward in session}")

    # Rolling Back
    # ------------

    # The Session has a Session.rollback() method that as expected emits a
    # ROLLBACK on the SQL connection in progress. However, it also has an
    # effect on the objects that are currently associated with the Session,
    # in our previous example the Python object sandy. While we changed the
    # .fullname of the sandy object to read "Sandy Squirrel", we want to roll
    # back this change. Calling Session.rollback() will not only roll back the
    # transaction but also expire all objects currently associated with this
    # Session, which will have the effect that they will refresh themselves
    # when next accessed using a process known as lazy loading.
    session.rollback()

    # To view the “expiration” process more closely, we may observe that the
    # Python object sandy has no state left within its Python __dict__, with
    # the exception of a special SQLAlchemy internal state object.
    print(f"sandy.__dict__: {sandy.__dict__}")

    # This is the “expired” state; accessing the attribute again will autobegin
    # a new transaction and refresh sandy with the current database row.
    print(f"sandy.fullname: {sandy.fullname}")

    # We may now observe that the full database row was also populated into the
    # __dict__ of the sandy object.
    print(f"sandy.__dict__: {sandy.__dict__}")

    # For deleted objects, when we earlier noted that patrick was no longer in
    # the session, that object’s identity is also restored.
    print(f"patrick in session: {patrick in session}")

    # and of course the database data is present again as well
    another_patrick = session.execute(
        select(User).where(User.name == "patrick")
    ).scalar_one()

    print(f"another_patrick is patrick: {another_patrick is patrick}")

    # Closing a Session
    # -----------------

    # Within the above sections we used a Session object outside of a Python
    # context manager, that is, we didn’t use the with statement. That’s fine,
    # however if we are doing things this way, it’s best that we explicitly
    # close out the Session when we are done with it.
    session.close()

    # Closing the Session, which is what happens when we use it in a context
    # manager as well, accomplishes the following things
    #
    # - It releases all connection resources to the connection pool, cancelling
    #   out (e.g. rolling back) any transactions that were in progress.
    #   This means that when we make use of a session to perform some read-only
    #   tasks and then close it, we don’t need to explicitly call upon
    #   Session.rollback() to make sure the transaction is rolled back; the
    #   connection pool handles this.
    # - It expunges all objects from the Session.
    #   This means that all the Python objects we had loaded for this Session,
    #   like sandy, patrick and squidward, are now in a state known as detached.
    #   In particular, we will note that objects that were still in an expired
    #   state, for example due to the call to Session.commit(), are now
    #   non-functional, as they don’t contain the state of a current row and
    #   are no longer associated with any database transaction in which to be
    #   refreshed.

    # print(f"squidward.name: {squidward.name}")
    # sqlalchemy.orm.exc.DetachedInstanceError: Instance <User at 0x7f414d66ab50> is not bound to a Session;

    # The detached objects can be re-associated with the same, or a new Session
    # using the Session.add() method, which will re-establish their relationship
    # with their particular database row.
    session.add(squidward)
    print(f"squidward.name: {squidward.name}")

    # Try to avoid using objects in their detached state, if possible. When the
    # Session is closed, clean up references to all the previously attached
    # objects as well. For cases where detached objects are necessary,
    # typically the immediate display of just-committed objects for a web
    # application where the Session is closed before the view is rendered, set
    # the Session.expire_on_commit flag to False.


if __name__ == "__main__":
    show_objects()

    dbname = "sqlite+pysqlite:///:memory:"
    engine = create_engine(dbname, echo=True, future=True)

    emit_database_DDL(engine)

    insert_first_users(engine)

    # Tutorial part
    insert_data(engine)

    updating_and_deleting_orm_objects(engine)
