"""
https://docs.sqlalchemy.org/en/14/orm/session_basics.html

Mieux :
https://docs.sqlalchemy.org/en/14/tutorial/index.html#unified-tutorial
https://docs.sqlalchemy.org/en/14/tutorial/engine.html#tutorial-engine
"""
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import declarative_base, Session


Base = declarative_base()


class Author(Base):
    """Simple Author class to be associated with an author table in the
    database.

    Arguments:
        Base {[type]} -- [description]
    """

    __tablename__ = "authors"

    id = Column(Integer, primary_key=True)
    name = Column(String)


class Book(Base):
    __tablename__ = "books"

    id = Column(Integer, primary_key=True)
    title = Column(String)


if __name__ == "__main__":
    # We’re using the name pysqlite, which in modern Python use is the sqlite3
    # standard library interface for SQLite. If omitted, SQLAlchemy will use a
    # default DBAPI for the particular database selected.
    # dbname = "sqlite+pysqlite:///:memory:"
    dbname = "sqlite+pysqlite:///session.1-4.db"
    engine = create_engine(dbname, echo=True, future=True)

    # TODO Use a new way ?
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    # create session and add objects
    with Session(engine) as session:
        with session.begin():
            barjavel = Author(name="René Barjavel")
            session.add(barjavel)

            ndt = Book(title="Nuit des temps")
            session.add(ndt)

        # inner context calls session.commit(), if there were no exceptions
    # outer context calls session.close()
