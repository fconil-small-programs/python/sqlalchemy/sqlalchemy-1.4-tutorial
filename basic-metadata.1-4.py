"""
https://docs.sqlalchemy.org/en/14/tutorial/index.html#unified-tutorial
https://docs.sqlalchemy.org/en/14/tutorial/metadata.html#tutorial-working-with-metadata

The most common foundational objects for database metadata in SQLAlchemy are
known as MetaData, Table, and Column.
"""
from sqlalchemy import MetaData
from sqlalchemy import Table, Column, Integer, String
from sqlalchemy import ForeignKey
from sqlalchemy import create_engine

metadata = MetaData()

user_table = Table(
    "user_account",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("name", String(30)),
    Column("fullname", String),
)

# When using the ForeignKey object within a Column definition, we can omit
# the datatype for that Column; it is automatically inferred from that of
# the related column.
address_table = Table(
    "address",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("user_id", ForeignKey("user_account.id"), nullable=False),
    Column("email_address", String, nullable=False),
)


def show_objects():
    print(user_table.c.name)
    print(user_table.c.keys())
    print(user_table.primary_key)


def emit_database_DDL(engine):
    # The DDL create process by default includes some SQLite-specific PRAGMA
    # statements that test for the existence of each table before emitting a
    # CREATE. The full series of steps are also included within a BEGIN/COMMIT
    # pair to accommodate for transactional DDL (SQLite does actually support
    # transactional DDL, however the sqlite3 database driver historically runs
    # DDL in “autocommit” mode).
    metadata.create_all(engine)


if __name__ == "__main__":
    show_objects()

    dbname = "sqlite+pysqlite:///:memory:"
    engine = create_engine(dbname, echo=True, future=True)

    emit_database_DDL(engine)
