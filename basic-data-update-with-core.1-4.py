"""
https://docs.sqlalchemy.org/en/14/tutorial/index.html#unified-tutorial

https://docs.sqlalchemy.org/en/14/tutorial/data_update.html

TODO remove all select code
"""

from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    MetaData,
    String,
    Table,
    bindparam,
    create_engine,
    delete,
    insert,
    select,
    update,
)
from sqlalchemy.dialects import mysql

metadata = MetaData()

user_table = Table(
    "user_account",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("name", String(30)),
    Column("fullname", String),
)

# When using the ForeignKey object within a Column definition, we can omit
# the datatype for that Column; it is automatically inferred from that of
# the related column.
address_table = Table(
    "address",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("user_id", ForeignKey("user_account.id"), nullable=False),
    Column("email_address", String, nullable=False),
)

some_table = Table(
    "fake_sql_table",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("x", Integer, nullable=False),
    Column("y", Integer, nullable=False),
)


def show_objects():
    print(user_table.c.name)
    print(user_table.c.keys())
    print(user_table.primary_key)


def emit_database_DDL(engine):
    # The DDL create process by default includes some SQLite-specific PRAGMA
    # statements that test for the existence of each table before emitting a
    # CREATE. The full series of steps are also included within a BEGIN/COMMIT
    # pair to accommodate for transactional DDL (SQLite does actually support
    # transactional DDL, however the sqlite3 database driver historically runs
    # DDL in “autocommit” mode).
    metadata.create_all(engine)


def insert_stmt(engine):
    stmt = insert(user_table).values(
        name="spongebob", fullname="Spongebob Squarepants"
    )

    with engine.connect() as conn:
        result = conn.execute(stmt)
        conn.commit()

        print(f"result.inserted_primary_key: {result.inserted_primary_key}")


def insert_two_rows(engine):
    with engine.connect() as conn:
        result = conn.execute(
            insert(user_table),
            [
                {"name": "sandy", "fullname": "Sandy Cheeks"},
                {"name": "patrick", "fullname": "Patrick Star"},
            ],
        )
        conn.commit()


def scalar_subqueries(engine):
    # see basic-insert-with-core.1-4.py
    # https://docs.sqlalchemy.org/en/14/glossary.html#term-scalar-subquery
    scalar_subquery = (
        select(user_table.c.id)
        .where(user_table.c.name == bindparam("username"))
        .scalar_subquery()
    )

    with engine.connect() as conn:
        result = conn.execute(
            insert(address_table).values(user_id=scalar_subquery),
            [
                {
                    "username": "spongebob",
                    "email_address": "spongebob@sqlalchemy.org",
                },
                {"username": "sandy", "email_address": "sandy@sqlalchemy.org"},
                {"username": "sandy", "email_address": "sandy@aol.com"},
                {
                    "username": "patrick",
                    "email_address": "patrick@gmail.com",
                },
            ],
        )
        conn.commit()


def insert_three_character_without_email(engine):
    # Enter a character whithout email address
    with engine.connect() as conn:
        result = conn.execute(
            insert(user_table),
            [
                {"name": "jack", "fullname": "Jack Beauregard"},
                {"name": "wendy", "fullname": "Wendy Darling"},
                {"name": "jim", "fullname": "Jim Carrey"},
            ],
        )
        conn.commit()


def update_stmt(engine):
    # Some backends support an UPDATE statement that may modify multiple tables
    # at once, and the UPDATE statement also supports RETURNING such that
    # columns contained in matched rows may be returned in the result set.
    # A basic UPDATE looks like :
    stmt = (
        update(user_table)
        .where(user_table.c.name == "patrick")
        .values(fullname="Patrick The Star")
    )
    print(f"stmt: {stmt}")

    # UPDATE supports all the major SQL forms of UPDATE, including updates
    # against expressions, where we can make use of Column expressions
    stmt = update(user_table).values(fullname="Username: " + user_table.c.name)
    print(f"stmt: {stmt}")

    # To support UPDATE in an “executemany” context, where many parameter
    # sets will be invoked against the same statement, the bindparam()
    # construct may be used to set up bound parameters; these replace the
    # places that literal values would normally go.
    stmt = (
        update(user_table)
        .where(user_table.c.name == bindparam("oldname"))
        .values(name=bindparam("newname"))
    )

    with engine.connect() as conn:
        result = conn.execute(
            stmt,
            [
                {"oldname": "jack", "newname": "ed"},
                {"oldname": "wendy", "newname": "mary"},
                {"oldname": "jim", "newname": "jake"},
            ],
        )
        print(f"{result.rowcount} updated rows")


def correlated_updates(engine):
    # An UPDATE statement can make use of rows in other tables by using a
    # correlated subquery. A subquery may be used anywhere a column expression
    # might be placed.
    scalar_subq = (
        select(address_table.c.email_address)
        .where(address_table.c.user_id == user_table.c.id)
        .order_by(address_table.c.id)
        .limit(1)
        .scalar_subquery()
    )

    # Mais on ne veut pas remplacer fullname par une adresse
    stmt = update(user_table).values(fullname=scalar_subq)
    print(f"stmt: {stmt}")


def update_from_stmt(engine):
    # Some databases such as PostgreSQL and MySQL support a syntax “UPDATE FROM”
    # where additional tables may be stated directly in a special FROM clause.
    # This syntax will be generated implicitly when additional tables are
    # located in the WHERE clause of the statement.
    stmt = (
        update(user_table)
        .where(user_table.c.id == address_table.c.user_id)
        .where(address_table.c.email_address == "patrick@gmail.com")
        .values(fullname="Pat")
    )
    print(f"PostgreSQL and MySQL stmt: {stmt}")

    # There is also a MySQL specific syntax that can UPDATE multiple tables.
    # This requires we refer to Table objects in the VALUES clause in order to
    # refer to additional tables.
    stmt = (
        update(user_table)
        .where(user_table.c.id == address_table.c.user_id)
        .where(address_table.c.email_address == "patrick@gmail.com")
        .values(
            {
                user_table.c.fullname: "Pat",
                address_table.c.email_address: "patrick@aol.com",
            }
        )
    )
    print(f"MySQL compiled stmt: {stmt.compile(dialect=mysql.dialect())}")


def parameter_ordered_update(engine):
    # nother MySQL-only behavior is that the order of parameters in the SET
    # clause of an UPDATE actually impacts the evaluation of each expression.
    # For this use case, the Update.ordered_values() method accepts a sequence
    # of tuples so that this order may be controlled.
    stmt = update(some_table).ordered_values(
        (some_table.c.y, 20),
        (some_table.c.x, some_table.c.y + 10),
    )
    print(f"MySQL stmt: {stmt}")
    print(f"MySQL compiled stmt: {stmt.compile(dialect=mysql.dialect())}")


def delete_stmt(engine):
    # The delete() statement from an API perspective is very similar to that of
    # the update() construct, traditionally returning no rows but allowing for
    # a RETURNING variant on some database backends.
    stmt = delete(user_table).where(user_table.c.name == "patrick")
    print(f"stmt: {stmt}")


def multiple_table_delete(engine):
    # Like Update, Delete supports the use of correlated subqueries in the WHERE
    # clause as well as backend-specific multiple table syntaxes, such as
    # DELETE FROM..USING on MySQL
    stmt = (
        delete(user_table)
        .where(user_table.c.id == address_table.c.user_id)
        .where(address_table.c.email_address == "patrick@gmail.com")
    )
    print(f"MySQL stmt: {stmt}")
    print(f"MySQL compiled stmt: {stmt.compile(dialect=mysql.dialect())}")


def get_affected_rowcount(engine):
    # Both Update and Delete support the ability to return the number of rows
    # matched after the statement proceeds, for statements that are invoked
    # using Core Connection, i.e. Connection.execute(). Per the caveats
    # mentioned below, this value is available from the CursorResult.rowcount
    # attribute.
    with engine.connect() as conn:
        result = conn.execute(
            update(user_table)
            .values(fullname="Patrick McStar")
            .where(user_table.c.name == "patrick")
        )
        print(f"Update rowcount: {result.rowcount}")

    # Facts about CursorResult.rowcount:
    #
    # - The value returned is the number of rows matched by the WHERE clause of
    #   the statement. It does not matter if the row were actually modified or not.
    # - CursorResult.rowcount is not necessarily available for an UPDATE or
    #   DELETE statement that uses RETURNING.
    # - For an executemany execution, CursorResult.rowcount may not be available
    #   either, which depends highly on the DBAPI module in use as well as
    #   configured options. The attribute CursorResult.supports_sane_multi_rowcount
    #   indicates if this value will be available for the current backend in use.
    # - Some drivers, particularly third party dialects for non-relational
    #   databases, may not support CursorResult.rowcount at all. The
    #   CursorResult.supports_sane_rowcount will indicate this.
    # - “rowcount” is used by the ORM unit of work process to validate that an
    #   UPDATE or DELETE statement matched the expected number of rows, and is
    #   also essential for the ORM versioning feature documented at Configuring
    #   a Version Counter.


def using_returning(engine):
    # Like the Insert construct, Update and Delete also support the RETURNING
    # clause which is added by using the Update.returning() and
    # Delete.returning() methods. When these methods are used on a backend that
    # supports RETURNING, selected columns from all rows that match the WHERE
    # criteria of the statement will be returned in the Result object as rows
    # that can be iterated.
    update_stmt = (
        update(user_table)
        .where(user_table.c.name == "patrick")
        .values(fullname="Patrick The Star")
        .returning(user_table.c.id, user_table.c.name)
    )
    print(f"Update stmt: {update_stmt}")

    delete_stmt = (
        delete(user_table)
        .where(user_table.c.name == "patrick")
        .returning(user_table.c.id, user_table.c.name)
    )
    print(f"Delete stmt: {delete_stmt}")


if __name__ == "__main__":
    show_objects()

    dbname = "sqlite+pysqlite:///:memory:"
    engine = create_engine(dbname, echo=True, future=True)

    emit_database_DDL(engine)

    insert_stmt(engine)
    insert_two_rows(engine)
    scalar_subqueries(engine)
    insert_three_character_without_email(engine)

    """
    update_stmt(engine)

    correlated_updates(engine)

    update_from_stmt(engine)

    parameter_ordered_update(engine)

    delete_stmt(engine)

    multiple_table_delete(engine)

    get_affected_rowcount(engine)
    """

    using_returning(engine)
