from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import declarative_base, sessionmaker


Base = declarative_base()


class Author(Base):
    """Simple Author class to be associated with an author table in the
    database.

    Arguments:
        Base {[type]} -- [description]
    """

    __tablename__ = "authors"
    id = Column(Integer, primary_key=True)
    name = Column(String)


if __name__ == "__main__":
    dbname = "sqlite:///session.db"
    # dbname = "sqlite:///:memory:"
    engine = create_engine(dbname)  # , echo=True)

    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    # The sessionmaker factory generates new Session objects when called,
    # creating them given the configurational arguments established here.

    # La classe sessionmaker() retourne une **instance** de sessionmaker
    # qui crée une instance de la classe Session() quand on l'appelle.
    # Ce qui est perturbant, c'est d'appeler "Session" l'instance de
    # sessionmaker !!!
    # cf sessionmaker.__class__ dans sqlalchemy/orm/session.py
    session_factory = sessionmaker(bind=engine)
    session = session_factory()

    author = Author(name="René Barjavel")

    session.add(author)
    session.commit()
